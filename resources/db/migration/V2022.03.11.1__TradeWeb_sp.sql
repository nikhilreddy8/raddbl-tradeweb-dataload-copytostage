create or replace procedure COPY_S3_FILE_TO_STAGING_SP(PROCESSING_UUID VARCHAR, STAGE_TABLE_NAME VARCHAR, S3_BUCKET VARCHAR, S3_KEY VARCHAR, AUDIT_DATA VARCHAR, STORAGE_INTEGRATION VARCHAR)
    returns FLOAT NOT NULL
    language javascript
    as
    $$
    		        
    const random_str_sql = 'select randstr(5, random()), date_part(epoch_second, getdate()), date_part(epoch_millisecond, getdate()) from table(generator(rowcount => 1));'
    random_str_stm = snowflake.createStatement({sqlText:random_str_sql});
    random_string_result = random_str_stm.execute();
    random_string_result.next();
    const random_str = random_string_result.getColumnValue(1);
	const batch_num = random_string_result.getColumnValue(2);
	const batch_timestamp = random_string_result.getColumnValue(3);
	
//------------------------------------------------------------------------------------------------------------//
    const stage_table_name = STAGE_TABLE_NAME
    const stage_table_name1 = '\"'+ STAGE_TABLE_NAME+ '\"';
    const stage_table_name_tmp =  stage_table_name + '_tmp_' + random_str
    const create_tmp_table_sql =  'create temporary table ' + stage_table_name_tmp + ' like ' + stage_table_name1 + ';'
//  const alter_table_remove_processing_id_sql = 'alter table ' + stage_table_name_tmp + ' drop column processing_uuid;'
    const alter_table_remove_processing_and_audit_columns_sql = 'alter table ' + stage_table_name_tmp + ' drop column processing_uuid, column processing_timestamp, column processing_audit_data, column processing_batch_num;'
    const alter_table_add_processing_id_sql = 'alter table ' + stage_table_name_tmp + ' add column processing_uuid text;'


    var copy_from_s3_sql  = ' copy into ' + stage_table_name_tmp + ' from '
        copy_from_s3_sql += ' s3://' + S3_BUCKET + '/' + S3_KEY + ' '
        copy_from_s3_sql += ' file_format = (TYPE = \'csv\' FIELD_DELIMITER = \',\' SKIP_HEADER = 1 TRIM_SPACE = TRUE ENCODING = UTF8 ESCAPE = NONE FIELD_OPTIONALLY_ENCLOSED_BY = \'\042\' ESCAPE_UNENCLOSED_FIELD=NONE)'
        copy_from_s3_sql += ' storage_integration = ' + STORAGE_INTEGRATION
        copy_from_s3_sql += ';'

    const update_processing_id_sql = 'update ' + stage_table_name_tmp + ' set processing_uuid=\'' + PROCESSING_UUID + '\';'
    const insert_into_stage_sql = "insert into "+ stage_table_name1 + " select *, to_timestamp(" + batch_timestamp + ", 3), to_variant('"+ AUDIT_DATA + "'), to_numeric(" + batch_num + ") from "+ stage_table_name_tmp + ";"

    const count_rows_sql = 'select count(*) from ' + stage_table_name1 + ' where processing_uuid=\'' + PROCESSING_UUID + '\';'
    const tmp_table_drop_sql = 'drop table if exists ' + stage_table_name_tmp + ';'
//------------------------------------------------------------------------------------------------------------//

//Creating Tmp Table//
    create_tmp_table_stmt = snowflake.createStatement({sqlText:create_tmp_table_sql});
    create_tmp_table_stmt.execute();

//Altering the Tmp Table to remove filed that are not in the file//
    alter_table_remove_processing_and_audit_columns_stmt = snowflake.createStatement({sqlText:alter_table_remove_processing_and_audit_columns_sql});
    alter_table_remove_processing_and_audit_columns_stmt.execute();
try {
    //Performing copy command//
        copy_from_s3_stmt = snowflake.createStatement({sqlText:copy_from_s3_sql});
        copy_from_s3_stmt.execute();

    //Altering the Tmp table to add back filed that needs to be updated manually//
        alter_tmp_table_add_processing_id_stmt = snowflake.createStatement({sqlText:alter_table_add_processing_id_sql});
        alter_tmp_table_add_processing_id_stmt.execute();

    //Updating the fields that needs to be inserted into stage table//
        update_processing_id_stmt = snowflake.createStatement({sqlText:update_processing_id_sql});
        update_processing_id_stmt.execute();

    //Insert the data back into staging table//
        insert_into_stage_stmt = snowflake.createStatement({sqlText:insert_into_stage_sql});
        insert_into_stage_stmt.execute();

    //Remove the Tmp table//
        tmp_table_drop_sql_stmt = snowflake.createStatement({sqlText:tmp_table_drop_sql});
        tmp_table_drop_sql_stmt.execute();

    //Read back the records processed//
        count_rows_stmt = snowflake.createStatement({sqlText:count_rows_sql});
        result = count_rows_stmt.execute();
        result.next();
        return result.getColumnValue(1);
 }
 catch (err)  {
        //Remove the Tmp table//
        tmp_table_drop_sql_stmt = snowflake.createStatement({sqlText:tmp_table_drop_sql});
        tmp_table_drop_sql_stmt.execute();
        throw err
        }
    
	$$
;