#!/usr/bin/env python
from setuptools import setup, find_packages

with open('README.md') as readme_file:
    README = readme_file.read()

with open('requirements.txt') as f:
    required = f.read().splitlines()

install_requires = required

setup(
    name="raddbl-tradeweb-dataload-copytostage", # Replace with your own username
    version="0.0.1",
    description="AWS Lambda function to copy from s3 to stage",
    long_description=README,
    author="ANG RADD",
    author_email="ivz-angraddteam@amvescap.net",
    url="",
    packages=find_packages(exclude=['tests']),
    install_requires=install_requires,
    include_package_data=True,
    zip_safe=False,
    keywords='raddbl-tradeweb-dataload-copytostage',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)