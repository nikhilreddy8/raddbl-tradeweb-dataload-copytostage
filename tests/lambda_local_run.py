import os

if __name__ == '__main__':
    class Context:
        def __init__(self):
            self.invoked_function_arn = "arn:aws:lambda:region:111111111111:function:test-function"

    os.environ["ENV"] = "dev"

    event = {
        "eventType": "s3",
        "s3": {
            "bucket": "ivz-dev-0026-student-landing-ue1",
            "key": "raw/EAGLE_EDL_SECMASTER_.txt",
            "processing_uuid": "UUID1"
        }
    }

    from lambda_function import lambda_handler

    response = lambda_handler(event, Context())

    print(response)