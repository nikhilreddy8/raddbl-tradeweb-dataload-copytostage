import os
import sys
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch

from angcore.pipeline.s3_copy_pipeline import S3CopyPipeline

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import lambda_function


class Context:
    def __init__(self, invoked_function_arn):
        self.invoked_function_arn = invoked_function_arn
        self.function_name = "test-function"


def final_response():
    return (200, {
        "eventInput": {
            "processing_uuid": "UUID1",
            "stageTable": "SREF_STAGE_PACE_SECMASTER",
            "s3Bucket": "ivz-dev-0041-srefsd-landing-ue1",
            "s3Key": "raw/EAGLE_EDL_SECMASTER_20201118121147.txt"}
    }
            )


def current_file_path():
    return str(Path(__file__).parent)


class TestLambda_handler(TestCase):
    @patch("angcore.pipeline.s3_copy_pipeline.S3CopyPipeline",
           return_value=S3CopyPipeline(os.path.join(Path(__file__).parent, "config/config.yaml")))
    @patch("angcore.pipeline.s3_copy_pipeline.S3CopyPipeline.execute_pipeline", return_value=final_response())
    @patch("os.path.join", return_value=current_file_path())
    def test_lambda_handler_when_event_context_passed_then_calls_pipline(self, mock_path, mock_execute, mock_pipeline):
        event = {
            "eventType": "s3",
            "s3": {
                "bucket": "ivz-dev-0041-srefsd-landing-ue1",
                "key": "raw/EAGLE_EDL_SECMASTER_20201118121147.txt",
                "processing_uuid": "UUID1"
            }
        }

        context = Context("arn:aws:secretsmanager:us-east-1:782258485841")

        response = lambda_function.lambda_handler(event, context)

        mock_pipeline.assert_called_with(os.path.join(Path(__file__).parent, "config/config.yaml"))
        mock_execute.assert_called_with(event, context.invoked_function_arn)
        self.assertEqual(response.get('statusCode'), 200, "StatusCode Check")
        self.assertEqual(response.get('body'), {
            "eventInput": {
                "processing_uuid": "UUID1",
                "stageTable": "SREF_STAGE_PACE_SECMASTER",
                "s3Bucket": "ivz-dev-0041-srefsd-landing-ue1",
                "s3Key": "raw/EAGLE_EDL_SECMASTER_20201118121147.txt"}
        }, "Response body Check")
