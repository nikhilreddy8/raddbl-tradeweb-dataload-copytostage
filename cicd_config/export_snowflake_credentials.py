from angcore.config_reader import load_config
import argparse
import os

parser = argparse.ArgumentParser(description='flyway deployment credentials')
parser.add_argument("--f",required=True,help="This is the filepath for config location")
args= parser.parse_args()
filepath= args.f
response = load_config(filepath)

with open("./export_vars", "w") as f:
    for key in response:
        f.writelines(f'export {key.upper().replace(".","_")}={response[key]}\n') if "snowflake" in key else None