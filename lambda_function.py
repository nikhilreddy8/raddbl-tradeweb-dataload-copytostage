from core_logger import get_logger, APP_TRACER
from angcore.pipeline.s3_copy_pipeline import S3CopyPipeline
from angcore import config_reader
from pathlib import Path
import json
import os
logger = get_logger(__name__)

def inline_sql_function(data_dict):
    return '''CALL LOAD.copy_s3_file_to_staging_sp('{processing_uuid}','{stage_table}','{bucket}','{key}', '{audit_data}', '{storage_integration}')'''.format_map(data_dict)


@APP_TRACER
def lambda_handler(event, context):
    try:
        logger.info("Started running lambda_handler")
        
        config_file_path = os.path.join(Path(__file__).parent, "config/config.yaml")
        configuration = config_reader.load_config(config_file=config_file_path) 
        pipeline = S3CopyPipeline(event, config_file_path)
        audit_data_dict_temp = json.dumps(pipeline.get_audit_data)
        data_dict = {
                    'processing_uuid': pipeline.get_processing_uuid,
                    'stage_table': pipeline.get_stage_table,
                    'bucket': pipeline.get_bucket,
                    #'key': pipeline.get_key,
                    'audit_data': audit_data_dict_temp.replace("'", "").replace('"',''),
                    #'storage_integration': configuration.get('storage.integration')
                    }
        (status_code, response_body) = pipeline.trigger_event(inline_sql_function, data_dict, '')
        lambda_response = __lambda_response__(status_code, response_body)
        logger.info(lambda_response)
        logger.info("Completed running lambda_handler")
        return lambda_response
    except Exception:
        logger.error("Error while executing lambda_handler", exc_info=True)
    raise Exception

def __lambda_response__(status_code, response_body):
    return {
        'statusCode': status_code,
        'body': response_body
    }